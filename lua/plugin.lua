----------------------------------------------
-- PLUGINS FILE                             --
-- Here you can add or remove (by simply    --
-- commenting out) plugins                  --
----------------------------------------------

-- Install packer if it isn't installed {
local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end
-- }

return require('packer').startup({function()
	-- Packer can manage itself, funny, isn't it?
	use 'wbthomason/packer.nvim'

	-------------------------------------------------------
	-- Distraction-free writing                          --
	-------------------------------------------------------
	-- by default, one activates the other automatically --
	-- ----------------------------------------------------
	use 'junegunn/goyo.vim'                              -- disable everything but the text
	use 'junegunn/limelight.vim'                         -- only highlight the current paragraph


	-------------------------------------------------------
	-- Markup/TODO things                                --
	-------------------------------------------------------
	use 'vimwiki/vimwiki'                                -- basically org-mode for vim
	use 'jceb/vim-orgmode'                               -- literally org-mode for vim
	use 'freitass/todo.txt-vim'                          -- simple plain text TODO
	use {'iamcco/markdown-preview.nvim',                 -- markdown preview
		run = 'cd app && yarn install',                  --
		}                                                --


	-------------------------------------------------------
	-- Languages                                         --
	-------------------------------------------------------
	use 'vim-scripts/fountain.vim'                       -- fountain language syntax
	use 'plasticboy/vim-markdown'                        -- markdown language syntax

	-------------------------------------------------------
	-- Utilities                                         --
	-------------------------------------------------------
	-- which-key {
	use {"folke/which-key.nvim",
	config = function()
	require("which-key").setup {
		plugins = {
		spelling = {
    	  enabled = true, 
   		  suggestions = 30, 
	}}}
	end}
	-- }

	use 'ervandew/supertab'                             -- better completion
	use 'glepnir/dashboard-nvim'                        -- cool splash screen

	------------------------------------------------------
	-- Core                                             --
	------------------------------------------------------
	-- Color Schemes
	use 'tjdevries/colorbuddy.vim'
	use 'tjdevries/gruvbuddy.nvim'
	use 'ishan9299/modus-theme-vim'
	use 'Th3Whit3Wolf/onebuddy'
	use 'marko-cerovac/material.nvim'
end,
config = {
  display = {open_fn = require('packer.util').float}
}})
