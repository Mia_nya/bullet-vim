"""""""""""""""""""""""""""""""""""""""""
" INIT FILE                             "
" This file basically only reads from   "
" others                                "
"""""""""""""""""""""""""""""""""""""""""

lua require('plugin')                    -- Load the plugins
lua require('keys')                 -- Load which-key mappings

" Function to source only if file exists {
function! SourceIfExists(file)
  if filereadable(expand(a:file))
    exe 'source' a:file
  endif
endfunction
" }

call SourceIfExists('~/.config/nvim/custom.vim')     " Load personal config
