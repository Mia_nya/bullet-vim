"""""""""""""""""""""""""""""""""""""""
" GENERAL CONFIGS FILE                "
"""""""""""""""""""""""""""""""""""""""
" Note: You shouldn't change this, you should make any change you want to
" the custom.vim file

" spell
set complete+=kspell

" set tab to be 4 spaces
set tabstop=4
set shiftwidth=4

set textwidth=0 " goyo workaround

" highlight the line in which the cursor is
hi CursorLine cterm=NONE ctermbg=242
set cursorline

" enter limelight and do some things when entering goyo
function! s:goyo_enter()
  set noshowcmd
  set scrolloff=999
  Limelight

  " quit vim even if you're on goyo
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave()
  set showcmd
  set scrolloff=8
  Limelight!

  " quit vim even if you're on goyo
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

let g:limelight_conceal_ctermfg = 240
let g:limelight_conceal_guifg = '#83a598'

" which-key config
set timeoutlen=0
hi WhichKeyFloat ctermbg=NONE guifg=NONE

" change splits styling
hi VertSplit guifg=reverse guifg=reverse cterm=reverse guifg=Gray
set fillchars+=vert:\ 

set scrolloff=8 " Slightly better looking scrolloff

" supertab
let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabContextDefaultCompletionType="<c-x><c-k>"

" markdown
let g:vim_markdown_strikethrough = 1
au FileType vimwiki setlocal syntax=markdown

" vimwiki
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}] " set vimwiki to use markdown

" splash screen
source ~/.config/nvim/extra/splashscreen.vim

" file explorer
let g:netrw_preview   = 1
let g:netrw_liststyle = 3
let g:netrw_winsize   = 30
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" other things
set noshowmode " don't show mode in ex command line, we have that in the statusline!
""""""""""""""""""""""
" Filetypes          "
""""""""""""""""""""""
au BufRead,BufNewFile *.fountain set filetype=fountain " fountain
