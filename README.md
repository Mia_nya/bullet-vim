# Bullet Vim
###### A Neovim config for writers
A minimalistic-ish config, specially made to write notes, stories, novels or whatever other
human-readable text you want!

## But... Why?
Well, I love vim. I can't code. I want to use vim. Let's use vim to write other things.

I also just love configuring things :D

## Features
- Fast (ish)
- Minimal (ish)
- Pretty (or at least I hope so)
- Feature full (it has everything you'll ever need, and more!)
- Easy to install (literally one command!)

## Quick start
### Requisites
- nvim 0.5
- a Linux system (tested on arch Linux)
- [Pandoc 2+](https://pandoc.org/index.html)
- Xelatex
- [Python 3+](https://www.python.org/downloads/)
	- also install `neovim` with pip
		
### Install
To install, run
```sh
./install.sh
```
(This will backup any previous neovim config you may have)

### Config
All your configurations should go in `~./config/nvim/custom.vim`

This file won't be replaced when you update Bullet Vim.

***

> "Consult your lawyer if you plan to use this"

\- Prince

Made with love (and vim)
